import tkinter as tk
import tkinter.filedialog
from tkinter import *
import numpy as np
from PIL import Image, ImageTk
import os
import copy
import median
import contrast


class Interface:
    """Class is responsible for the user interface and program's logic."""
    def __init__(self):
        self.images_to_process = []
        self.state = 0  # Current program's state.
        self.display_index = 0  # Index of the displayed image.
        self.pil_images = []    # Images saved in PIL format.
        self.pil_images_previous = []   # Images from a previous step.

        self.isReturned = True  # Defines if a step back action has been performed.
        self.valid_directory = False    # Defines if a valid directory has been selected.
        self.last_step_performed = False    # Defines if the last step was performed.

        self.window = tk.Tk()
        self.window.title("Glottis detection")
        self.window.geometry("662x750")
        self.canvas = Canvas(self.window, width=512, height=512, background="grey")
        self.label = tk.Label(text="Glottis detection")
        self.dimensions = tk.Label(text="Image dimensions")

        self.xMinLabel = tk.Label(text="X min crop:")
        self.yMinLabel = tk.Label(text="Y min crop:")
        self.xMinEntry = tk.Entry(self.window)
        self.yMinEntry = tk.Entry(self.window)

        self.xMaxLabel = tk.Label(text="X max crop:")
        self.yMaxLabel = tk.Label(text="Y max crop:")
        self.xMaxEntry = tk.Entry(self.window)
        self.yMaxEntry = tk.Entry(self.window)

        self.save_button = tk.Button(text="Save", command=self.save_images)
        self.directory_button = tk.Button(text="Select directory", command=self.select_directory)

        self.restart_button = tk.Button(text="New detection", command=self.restart_program)

    def select_directory(self):
        if self.valid_directory is False:
            directory = tk.filedialog.askdirectory()
            if directory != '':
                self.check_images(directory)

    def hide_directory(self):
        self.directory_button.grid_remove()

    def check_images(self, directory):
        for file in os.listdir(directory):
            if file.endswith(".png") or file.endswith(".bmp") or file.endswith(".jpg") or file.endswith(".jpeg"):
                self.valid_directory = True
                self.hide_directory()
                file = directory + "/" + file
                self.images_to_process.append(file)

        if self.valid_directory is True:
            self.load_pil_images()
            self.show_image(0)

    def load_pil_images(self):
        for image_file in self.images_to_process:
            image = Image.open(image_file)
            self.pil_images.append(image)

    def start_processing(self):
        """Action performs one step in the image processing."""
        performed = False   # Defines if a step was really performed.

        if self.state == 0:     # If a valid directory has been selected, ROI will be performed.
            if self.valid_directory is True:
                self.region_of_interest()
                self.display_message("Region of interest performed")
                self.remove_entry()     # Removes the entry input fields.
                performed = True
        elif self.state == 1:   # Performs a median cut.
            previous_deepcopy = copy.deepcopy(self.pil_images_previous)     # Deepcopy of the previous images.
            try:
                self.process_median()
                self.display_message("Median cut performed")
                performed = True
            except ValueError:
                self.display_message("Value error")
                self.pil_images_previous = previous_deepcopy
        elif self.state == 2:   # Performs a histogram stretch.
            if self.last_step_performed is False:   # If the last step has not been performed.
                self.stretch_images()
                self.show_save()    # Shows a save button.
                self.last_step_performed = True
            performed = True

        if self.state <= 2 and performed is True:
            self.state += 1

        if performed is True:
            self.isReturned = False
            self.show_image(0)
            self.display_index = 0

    def return_action(self):
        """If an action was performed, step back will be made."""
        if self.isReturned is False:
            self.isReturned = True
            self.pil_images = copy.deepcopy(self.pil_images_previous)
            self.show_image(0)
            new_state = self.state - 1
            self.state = new_state
            self.display_message("Returned one step back")

            if self.state == 0:
                self.create_entry()
            elif self.state == 2:
                self.remove_save()
                self.last_step_performed = False

    def region_of_interest(self):
        self.save_pil_deepcopy()
        index = 0

        x_min_string = str(self.xMinEntry.get())
        y_min_string = str(self.yMinEntry.get())
        x_max_string = str(self.xMaxEntry.get())
        y_max_string = str(self.yMaxEntry.get())

        x_min = self.process_entry(x_min_string)
        y_min = self.process_entry(y_min_string)
        x_max = self.process_entry(x_max_string)
        y_max = self.process_entry(y_max_string)

        for image in self.pil_images:
            if x_min is not False and y_min is not False and x_max is not False and y_max is not False:
                width, height = self.pil_images[index].size
                self.crop_image(x_min, y_min, x_max, y_max, width, height, index)
            index += 1

    def crop_image(self, x_min, y_min, x_max, y_max, width, height, index):
        if 0 <= x_min < (x_max - 1) and 0 <= y_min < (y_max - 1) and x_max <= width and y_max <= height:
            self.pil_images[index] = self.pil_images[index].crop((x_min, y_min, x_max, y_max))

    def process_entry(self, entry_string):
        if self.check_value(entry_string) is True:
            value = int(entry_string)
            if value >= 0:
                return value
            else:
                return False
        else:
            return False

    def check_value(self, string):
        try:
            int(string)
            return True
        except ValueError:
            return False

    def save_pil_deepcopy(self):
        self.pil_images_previous = copy.deepcopy(self.pil_images)

    def process_median(self):
        self.save_pil_deepcopy()

        if len(self.images_to_process) > 0:
            index = 0
            for image in self.pil_images:
                self.image_median(index)
                index += 1

    def image_median(self, index):
        image = np.asarray(self.pil_images[index].convert('LA'))
        processed_image = median.MedianCut(image).median_cut()

        self.pil_images[index] = Image.fromarray(processed_image)

    def stretch_images(self):
        self.save_pil_deepcopy()

        index = 0
        for image in self.pil_images:
            array_image = np.asanyarray(self.pil_images[index])
            array_image = contrast.Contrast(copy.deepcopy(array_image)).contrast()
            self.pil_images[index] = Image.fromarray(array_image)
            index += 1

    def show_image(self, index):
        """Shows an image with the given index."""
        resized_image = copy.deepcopy(self.pil_images[index])
        width, height = resized_image.size
        new_width = 0
        new_height = 0
        if width >= height:
            new_width, new_height = self.calculate_resize(width, height)
        else:
            new_height, new_width = self.calculate_resize(height, width)

        resized_image = resized_image.resize((new_width, new_height), Image.ANTIALIAS)
        tkinter_image = ImageTk.PhotoImage(resized_image)

        self.canvas.create_image(256, 256, anchor=CENTER, image=tkinter_image)
        self.canvas.image = tkinter_image
        self.dimensions.config(text="Image width: " + str(width) + " Image height: " + str(height))
        self.window.update()

    def calculate_resize(self, higher, lower):
        ratio = 512 / higher
        new_lower = int(lower * ratio)
        return 512, new_lower

    def show_left(self):
        """Action for a button to show next left image."""
        new_index = self.display_index - 1
        if new_index >= 0:
            self.show_image(new_index)
            self.display_index = new_index

    def show_right(self):
        """Action for a button to show next right image."""
        new_index = self.display_index + 1
        if new_index < len(self.pil_images):
            self.show_image(new_index)
            self.display_index = new_index

    def save_images(self):
        """Saves the processed images as PNG files."""
        directory = tk.filedialog.askdirectory()
        index = 0
        for image in self.pil_images:
            image.save(directory + "/detection_" + str(index) + ".png")
            index += 1

    def create_entry(self):
        """Creates the entry input fields in the layout."""
        self.xMinLabel.grid(row=5)
        self.xMinEntry.grid(row=5, column=1, sticky=W)

        self.yMinLabel.grid(row=6)
        self.yMinEntry.grid(row=6, column=1, sticky=W)

        self.xMaxLabel.grid(row=7)
        self.xMaxEntry.grid(row=7, column=1, sticky=W)

        self.yMaxLabel.grid(row=8)
        self.yMaxEntry.grid(row=8, column=1, sticky=W)

    def remove_entry(self):
        """Removes the entry input fields from the layout."""
        self.xMinLabel.grid_remove()
        self.xMinEntry.grid_remove()

        self.yMinLabel.grid_remove()
        self.yMinEntry.grid_remove()

        self.xMaxLabel.grid_remove()
        self.xMaxEntry.grid_remove()

        self.yMaxLabel.grid_remove()
        self.yMaxEntry.grid_remove()

    def show_save(self):
        self.save_button.grid(row=3, column=1, sticky=E)

    def remove_save(self):
        self.save_button.grid_remove()

    def display_message(self, string):
        """Displays a message in the upper label."""
        self.label.config(text=string)

    def restart_program(self):
        """Restarts the program to start a new detection."""
        os.execl(sys.executable, os.path.abspath(__file__), *sys.argv)

    def create_interface(self):
        """Constructs the user interface layout."""
        start_button = tk.Button(text="Step forward", command=self.start_processing)
        back_button = tk.Button(text="Return one step back", command=self.return_action)
        left_button = tk.Button(text="Move left", command=self.show_left)
        right_button = tk.Button(text="Move right", command=self.show_right)

        self.label.grid(row=0, column=1)
        self.dimensions.grid(row=1, column=1)
        self.canvas.grid(row=2, column=1)

        left_button.grid(row=2, column=0, padx=5)
        right_button.grid(row=2, column=2)

        self.directory_button.grid(row=3, column=1)

        start_button.grid(row=4, column=1, sticky=W)
        back_button.grid(row=4, column=1, sticky=E)

        self.restart_button.grid(row=9, column=1)

        self.create_entry()
        self.window.mainloop()
