import numpy as np


class MedianCut:
    """Class performs median cut segmentation."""
    def __init__(self, input_image):
        self.image = input_image
        self.rows, self.columns, color = input_image.shape
        self.array = []
        self.mapping = {}   # mapping table
        self.output = np.copy(input_image)

        for i in range(self.rows):
            for j in range(self.columns):
                self.array.append(input_image[i][j])

    def median_cut(self):
        """Performs the median cut segmentation (16 colors)"""
        image_array = self.array
        # Cuts the image recursively at the median 4 times to convert it to 16 colors at grayscale.
        self.perform_cutting(image_array, 0, 4)
        return self.median_image()

    def median_image(self):
        for i in range(self.rows):
            for j in range(self.columns):
                pixel_v = self.image[i][j]
                self.output[i][j] = self.mapping[tuple(pixel_v)]

        return self.output

    def perform_cutting(self, image_arr, index, limit):
        """Recursive function to create mapping table according to median cut algorithm."""
        if index >= limit:
            return
        index += 1

        self.sort_array(image_arr)
        lens = len(image_arr)
        left = image_arr[0:int(lens / 2)]   # Divides the sorted array in its median value.
        right = image_arr[int(lens / 2):lens]

        self.calculate_mapping(left)    # Assign values in mapping table.
        self.calculate_mapping(right)

        self.perform_cutting(left, index, limit)    # Cuts both halves of the array.
        self.perform_cutting(right, index, limit)

    def sort_array(self, image_arr):
        """Sorts pixels in an array by maximum difference value."""
        difference = np.amax(image_arr, axis=0) - np.amin(image_arr, axis=0)
        index = np.argmax(difference)
        image_arr.sort(key=lambda x: x[index])

    def calculate_mapping(self, image_arr):
        """Calculates the mean value of a color quantum to be assigned to a mapping table."""
        value = np.mean(image_arr, axis=0)
        for pixel in image_arr:
            self.mapping[tuple(pixel)] = value   # Assign the value to the mapping table.
