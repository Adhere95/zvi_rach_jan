import interface


def main():
    """Creates the program's window."""
    interface.Interface().create_interface()


if __name__ == "__main__":
    main()
