
class Contrast:
    """Class handles a function to highlight a quantum with the lowest brightness in an image."""
    def __init__(self, input_image):
        self.image = input_image
        self.rows, self.cols, color = input_image.shape

    def contrast(self):
        """Finds the minimum of an image and highlights the lowest quantum."""
        minimum = 255

        for i in range(self.rows):
            for j in range(self.cols):

                value = self.image[i][j][0]
                if value < minimum:
                    minimum = value

        limit = minimum

        for i in range(self.rows):
            for j in range(self.cols):
                value = self.image[i][j][0]
                new_value = 0
                if value > limit:
                    new_value = 255
                else:
                    new_value = 0
                self.image[i][j][0] = new_value

        return self.image
